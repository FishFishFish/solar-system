#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 24 18:23:36 2017

@author: jakub
"""

import pygame as g
import sys
import math


g.font.init()
screen = g.display.set_mode((1000, 1000), g.DOUBLEBUF)
background = (0, 0, 0)#(59, 69, 84)
labelFont = g.font.Font(None, 15)


class CelestialBody:
    def __init__(self, name,  width, color, speed, rad, orbit, orbit_around):
        self.name = name
        self.width = width
        self.color = color
        self.speed = speed
        self.orbit = orbit
        self.orbit_around = orbit_around
        self.position = [orbit_around[0]+orbit,orbit_around[1]]
        self.rad = rad  # todo static rad for all(pos)/ recalculate pos based on rad

    def draw(self):
        g.draw.circle(screen, self.color, (int(self.position[0]), int(self.position[1])), self.width)


    def move(self):
        self.rad += self.speed
        '''
        if self.rad == 2:
            self.rad = 0
        if self.rad > 2:
            difference = 2 - self.rad
            difference *= -1
            self.rad = 0
            self.rad += difference
        '''
        self.position[0] = self.orbit_around[0] + self.orbit * math.cos(self.rad)
        self.position[1] = self.orbit_around[1]+ self.orbit * math.sin(self.rad)

    def label(self):
        label_name = labelFont.render(self.name, 1, (255, 255, 255))
        label_position = labelFont.render(str(round_list(self.position)), 1, (255, 255, 255))
        screen.blit(label_name, (int(self.position[0] + self.width + 5), int(self.position[1] - 10)))
        screen.blit(label_position, (int(self.position[0] + self.width + 5), int(self.position[1])))


def round_list(l):
    return [int(round(i, 4)) for i in l]


Sun = CelestialBody("Sun", 20, (255, 255, 255), 0, 0, 0, [500, 500])
Mercury = CelestialBody("Mercury", 2, (198, 158, 13), 0.005, 0, 100, [500, 500])
Venus = CelestialBody("Venus", 3, (198, 158, 13), 0.004, 0, 200, [500, 500])
Earth = CelestialBody("Earth", 4, (41, 118, 242), 0.001, 0, 300, [500, 500])
Mars = CelestialBody("Mars", 3, (244, 176, 66), 0.0009, 0, 350, [500, 500])
Jupiter = CelestialBody("Jupiter", 10, (255, 193, 50), 0.0005, 0, 420, [500, 500])
Saturn = CelestialBody("Saturn", 8, (255, 210, 137), 0.0004, 0, 460, [500, 500])
Uranus = CelestialBody("Uranus", 7, (79, 171, 221), 0.0003, 0, 500, [500, 500])
Neptune = CelestialBody("Neptune", 6, (1, 124, 191), 0.0002, 0, 540, [500, 500])
Moon = CelestialBody("Moon", 1, (158, 158, 158), 0.05, 0, 50, Earth.position)
Enceladus = CelestialBody("Enceladus",1,(240,240,240),0.001,0,30,Saturn.position)
labels = True

while True:
    screen.fill(background)
    Sun.draw()
    Earth.move()
    Moon.orbit_around = Earth.position
    Moon.move()
    Moon.draw()
    Enceladus.move()
    Enceladus.draw()
    Earth.draw()
    Mercury.move()
    Mercury.draw()
    Venus.move()
    Venus.draw()
    Mars.move()
    Mars.draw()
    Jupiter.move()
    Jupiter.draw()
    Saturn.move()
    Saturn.draw()
    Uranus.move()
    Uranus.draw()
    Neptune.move()
    Neptune.draw()
    if labels:
        Sun.label()
        Mercury.label()
        Earth.label()
        Moon.label()
        Enceladus.label()
        Venus.label()
        Jupiter.label()
        Mars.label()
        Saturn.label()
        Neptune.label()
        Uranus.label()
    g.display.update()

    # Close
    for event in g.event.get():
        if event.type == g.QUIT:
            g.quit()
            sys.exit()
